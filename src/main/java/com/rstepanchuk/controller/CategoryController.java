package com.rstepanchuk.controller;

import com.rstepanchuk.controller.external.model.CategoryDTO;
import com.rstepanchuk.model.Category;
import com.rstepanchuk.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public ResponseEntity<List<CategoryDTO>> getAll(){
            ResponseEntity<List<CategoryDTO>> result;
            List<CategoryDTO> categories = categoryService.getAll().stream()
                    .map(CategoryDTO::of)
                    .collect(Collectors.toList());
            if (!categories.isEmpty()) {
                result = ResponseEntity.ok(categories);
            } else {
                result = new ResponseEntity<>(categories, NOT_FOUND);
            }
            return result;
    }

    @RequestMapping(value = "/categories/{id}", method = RequestMethod.GET)
    public ResponseEntity<Category> get(@PathVariable Long id){
        ResponseEntity<Category> result;
        Optional<Category> category = categoryService.get(id);
        if (category.isPresent()) {
            result = ResponseEntity.ok(category.get());
        } else {
            result = new ResponseEntity<>(null, NOT_FOUND);
        }
        return result;
    }

    @RequestMapping(value = "/categories", method = RequestMethod.POST)
    public ResponseEntity<Category> add(@RequestBody Category category) {
        return ResponseEntity.ok(categoryService.add(category));
    }

    @RequestMapping(value = "/categories", method = RequestMethod.PUT)
    public ResponseEntity<Category> get(@RequestBody Category category){
        return ResponseEntity.ok(categoryService.update(category));
    }

    @RequestMapping(value = "categories/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> delete(@PathVariable Long id){
        categoryService.delete(id);
        return ResponseEntity.ok(true);
    }


}

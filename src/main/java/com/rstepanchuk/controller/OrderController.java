package com.rstepanchuk.controller;

import com.rstepanchuk.model.Order;
import com.rstepanchuk.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(value="/orders", method = RequestMethod.GET)
    public ResponseEntity<List<Order>> getAll(){
        ResponseEntity<List<Order>> result;
        List<Order> orders = orderService.getAll();
        if (orders.isEmpty()) {
            result = new ResponseEntity<>(orders, NOT_FOUND);
        } else {
            result = ResponseEntity.ok(orders);
        }
        return result;
    }

    @RequestMapping(value="/orders/{id}", method = RequestMethod.GET)
    public ResponseEntity<Order> get(@PathVariable Long id) {
        return orderService.get(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @RequestMapping(value="/orders/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Order> delete(@PathVariable Long id) {
        return orderService.delete(id)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @RequestMapping(value="/orders/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Order> update(@RequestBody Order order, @PathVariable Long id) {
        order.setId(id);
        return ResponseEntity.ok(orderService.save(order));
    }

    @RequestMapping(value="/orders", method = RequestMethod.POST)
    public ResponseEntity<Order> add(@RequestBody List<Long> productsId) {
        return ResponseEntity.ok(orderService.createOrder(productsId));
    }
}

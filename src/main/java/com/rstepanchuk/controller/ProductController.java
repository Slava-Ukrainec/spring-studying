package com.rstepanchuk.controller;

import com.rstepanchuk.controller.external.model.ProductDTO;
import com.rstepanchuk.model.Product;
import com.rstepanchuk.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
public class ProductController {

    @Qualifier("productService-based")
    @Autowired
    ProductService productService;

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public ResponseEntity<List<ProductDTO>> getAll() {

        return productService.getAll().stream()
                .map(ProductDTO::of)
                .collect(collectingAndThen(toList(), l -> l.isEmpty()
                        ? new ResponseEntity<>(l, NOT_FOUND)
                        : ResponseEntity.ok(l)));

//        ResponseEntity<List<Product>> result;
//        List<Product> products = productService.getAll();
//        if (!products.isEmpty()) {
//            result = ResponseEntity.ok(products);
//        } else {
//            result = new ResponseEntity<>(products, NOT_FOUND);
//        }
//        return result;
    }

    @RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
    public ResponseEntity<ProductDTO> getById(@PathVariable Long id){
        return productService.get(id)
                .map(ProductDTO::of)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }

    @RequestMapping(value = "/products", method = RequestMethod.POST)
    public ResponseEntity<ProductDTO> add(@RequestBody ProductDTO dto) {
        Product product = productService.add(Product.of(dto));
        return ResponseEntity.ok(ProductDTO.of(product));
    }

}

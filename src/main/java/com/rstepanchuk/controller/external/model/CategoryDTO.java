package com.rstepanchuk.controller.external.model;

import com.rstepanchuk.model.Category;

import java.util.List;
import java.util.stream.Collectors;

public class CategoryDTO {

    private String categoryCode;
    private String name;
    private String description;
    private List<ProductDTO> products;

    public CategoryDTO() {
    }

    public CategoryDTO(String categoryCode, String name, String description, List<ProductDTO> products) {
        this.categoryCode = categoryCode;
        this.name = name;
        this.description = description;
        this.products = products;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }

    public static CategoryDTO of(Category category) {

        List<ProductDTO> productDTOS = category.getProducts().stream()
                .map(ProductDTO::of)
                .collect(Collectors.toList());

        return new CategoryDTO(category.getCategoryCode(),
                category.getName(),
                category.getDescription(),
                productDTOS);
    }
}

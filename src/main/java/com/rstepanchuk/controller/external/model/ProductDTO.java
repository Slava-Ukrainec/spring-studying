package com.rstepanchuk.controller.external.model;

import com.rstepanchuk.model.Product;

public class ProductDTO {

    private String productCode;
    private String name;
    private String description;
    private Double price;
    private String categoryName;

    public ProductDTO() {
    }

    public ProductDTO(String productCode, String name, String description, Double price, String categoryName) {
        this.productCode = productCode;
        this.name = name;
        this.description = description;
        this.price = price;
        this.categoryName = categoryName;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Double getPrice() {
        return price;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public static ProductDTO of(Product product) {
        return new ProductDTO(
                product.getProductCode(),
                product.getName(),
                product.getDescription(),
                product.getPrice(),
                product.getCategory().getName()
        );
    }
}

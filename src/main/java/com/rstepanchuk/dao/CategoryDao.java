package com.rstepanchuk.dao;

import com.rstepanchuk.model.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryDao {

    List<Category> getAll();
    Category add(Category category);
    Optional<Category> get(Long categoryID);
    void delete(Long id);
    Category update(Category category);
}

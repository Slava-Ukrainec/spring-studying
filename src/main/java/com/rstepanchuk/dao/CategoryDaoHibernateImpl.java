package com.rstepanchuk.dao;

import com.rstepanchuk.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public class CategoryDaoHibernateImpl implements CategoryDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Category> getAll() {
        return entityManager
                .createQuery("from Category", Category.class)
                .getResultList();
    }

    @Override
    public Category add(Category category) {
        entityManager.persist(category);
        return category;
    }

    @Override
    public Optional<Category> get(Long categoryID) {
        Category category = entityManager.find(Category.class, categoryID);
        return Optional.ofNullable(category);
    }

    @Override
    public void delete(Long id) {
        Category category = entityManager.find(Category.class, id);
        entityManager.remove(category);
    }

    @Override
    public Category update(Category category) {
        return entityManager.merge(category);
    }
}

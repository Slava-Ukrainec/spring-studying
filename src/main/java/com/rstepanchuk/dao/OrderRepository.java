package com.rstepanchuk.dao;

import com.rstepanchuk.model.Order;
import com.rstepanchuk.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}

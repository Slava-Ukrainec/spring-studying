package com.rstepanchuk.dao;

import com.rstepanchuk.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductDao {
    List<Product> getAll();
    Product add(Product product);
    Optional<Product> get(Long id);
}

package com.rstepanchuk.dao;

import com.rstepanchuk.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository("hibernate-based")
public class ProductDaoHibernateImpl implements ProductDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<Product> getAll() {
        return entityManager
                .createQuery("from Product", Product.class)
                .getResultList();
    }

    @Override
    public Product add(Product product) {
        entityManager
                .persist(product);
        return product;
    }

    @Override
    public Optional<Product> get(Long id) {
        Product product = entityManager.find(Product.class, id);
        return Optional.ofNullable(product);
    }
}

package com.rstepanchuk.dao;

import com.rstepanchuk.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository("jdbcTemplate-based")
public class ProductDaoJdbsTemplateImpl implements ProductDao{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Product> getAll() {
        String query = "SELECT ID, NAME, DESCRIPTION, PRICE, PRODUCT_CODE FROM PRODUCTS";
        return jdbcTemplate.query(query, (rs, rowNum) ->
             new Product(
                    rs.getLong(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getDouble(4),
                     rs.getString(5)));
    }

    @Override
    public Product add(Product product) {
        String query = "INSERT INTO PRODUCTS VALUES (?, ?, ?, ?, ?)";
        Product result = new Product(null, product.getName(), product.getDescription(), product.getPrice(), product.getProductCode());

        return jdbcTemplate.execute(query, (PreparedStatementCallback<Product>) ps -> {
            ps.setNull(1, Types.BIGINT);
            ps.setString(2, product.getName());
            ps.setString(3, product.getDescription());
            ps.setDouble(4, product.getPrice());
            ps.setString(5, product.getProductCode());
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();

            if (rs.next()) {
                result.setId(rs.getLong(1));
            }

            return result;
        });
    }

    @Override
    public Optional<Product> get(Long id) {
        throw new RuntimeException("Method is not ready yet");
    }
}

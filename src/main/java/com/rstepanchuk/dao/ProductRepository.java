package com.rstepanchuk.dao;

import com.rstepanchuk.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {

}

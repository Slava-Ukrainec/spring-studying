package com.rstepanchuk.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="ORDERS")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "AMOUNT")
    private Double amount;

    @ManyToMany
    @JoinTable(
            name = "ORDERS_TO_PRODUCTS",
            joinColumns = {@JoinColumn(name = "FK_ORDER_ID")},
            inverseJoinColumns = {@JoinColumn(name = "FK_PRODUCT_ID")}
            )
    private List<Product> products = new ArrayList<>();

    public Order() {
    }

    public Order(Long id, Double amount, List<Product> products) {
        this.id = id;
        this.amount = amount;
        this.products = products;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}

package com.rstepanchuk.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.rstepanchuk.controller.external.model.ProductDTO;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PRODUCTS")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME")
    private String name;
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "PRICE")
    private Double price;
    @Column(name = "PRODUCT_CODE")
    private String productCode;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "FK_CATEGORY_ID")
    private Category category;

    @ManyToMany(mappedBy = "products")
    @JsonBackReference
    private List<Order> orders = new ArrayList<>();

    public Product(){};

    public Product(String name, String description, Double price, String productCode) {
        this(null, name, description, price, productCode);
    }

    public Product(Long id, String name, String description, Double price, String productCode) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.productCode = productCode;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public static Product of (ProductDTO dto) {
        return new Product(
                dto.getName(),
                dto.getDescription(),
                dto.getPrice(),
                dto.getProductCode());
    }
}
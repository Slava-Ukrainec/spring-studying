package com.rstepanchuk.service;

import com.rstepanchuk.model.Category;

import java.util.List;
import java.util.Optional;
import java.util.stream.DoubleStream;

public interface CategoryService {
    List<Category> getAll();
    Category add(Category category);
    Optional<Category> get(Long categoryID);
    void delete(Long id);
    Category update(Category category);
}

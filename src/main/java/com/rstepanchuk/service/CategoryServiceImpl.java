package com.rstepanchuk.service;

import com.rstepanchuk.dao.CategoryDao;
import com.rstepanchuk.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    @Override
    public List<Category> getAll() {
        return categoryDao.getAll();
    }

    @Override
    public Category add(Category category) {
        return categoryDao.add(category);
    }

    @Override
    public Optional<Category> get(Long categoryID) {
        return categoryDao.get(categoryID);
    }

    @Override
    public void delete(Long id) {
        categoryDao.delete(id);
    }

    @Override
    public Category update(Category category) {
        return categoryDao.update(category);
    }
}

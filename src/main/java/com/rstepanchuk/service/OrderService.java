package com.rstepanchuk.service;

import com.rstepanchuk.model.Order;

import java.util.List;
import java.util.Optional;
import java.util.stream.DoubleStream;

public interface OrderService {
    List<Order> getAll();
    Optional<Order> get(Long id);
    Optional<Order> delete(Long id);
    Order save(Order order);
    Order createOrder(List<Long> productsId);
}

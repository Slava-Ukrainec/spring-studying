package com.rstepanchuk.service;

import com.rstepanchuk.dao.OrderRepository;
import com.rstepanchuk.model.Order;
import com.rstepanchuk.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Qualifier("productService-based")
    @Autowired
    private ProductService productService;

    @Override
    public List<Order> getAll() {
        return orderRepository.findAll();
    }

    @Override
    public Optional<Order> get(Long id) {
        return orderRepository.findById(id);
    }

    @Override
    public Optional<Order> delete(Long id) {
        Optional<Order> order = get(id);
        if (order.isPresent()) {
            orderRepository.deleteById(id);
            return order;
        }
        return Optional.empty();
    }

    @Override
    public Order save(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public Order createOrder(List<Long> productsId) {
        return productsId.stream()
                .map(productService::get)
                .map(o -> o.orElseThrow(() -> new IllegalArgumentException("One or more product Id's are invalid")))
                .collect(Collectors.collectingAndThen(toList(),ps -> {
                    double amount = ps.stream().mapToDouble(Product::getPrice).sum();
                    return orderRepository.save(new Order (null, amount, ps));
                }));
    }
}

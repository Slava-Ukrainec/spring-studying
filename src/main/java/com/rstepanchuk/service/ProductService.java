package com.rstepanchuk.service;

import com.rstepanchuk.model.Product;

import java.util.List;
import java.util.Optional;
import java.util.stream.DoubleStream;

public interface ProductService {
    List<Product> getAll();
    Product add(Product product);
    Optional<Product> get(Long id);
}

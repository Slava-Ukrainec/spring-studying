package com.rstepanchuk.service;


import com.rstepanchuk.dao.ProductDao;
import com.rstepanchuk.dao.ProductRepository;
import com.rstepanchuk.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductServiceDaoImpl implements ProductService {

    @Autowired
    @Qualifier("hibernate-based")
    private ProductDao productDao;


    public List<Product> getAll() {
        return productDao.getAll();
    }

    @Override
    public Product add(Product product) {
        product.setProductCode(generateProductCode());
        return productDao.add(product);
    }

    @Override
    public Optional<Product> get(Long id) {
        return productDao.get(id);
    }

    private String generateProductCode(){
        return UUID.randomUUID().toString();
    }
}

package com.rstepanchuk.service;


import com.rstepanchuk.dao.ProductRepository;
import com.rstepanchuk.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service("productService-based")
public class ProductServiceImpl implements ProductService {

//    @Autowired
//    @Qualifier("hibernate-based")
//    private ProductDao productDao;
    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAll() {
        return productRepository.findAll();
    }

    @Override
    public Product add(Product product) {
        product.setProductCode(generateProductCode());
        return productRepository.save(product);
    }

    @Override
    public Optional<Product> get(Long id) {
        return productRepository.findById(id);
    }

    private String generateProductCode(){
        return UUID.randomUUID().toString();
    }
}
